export interface Configuration {
	host: string
	port: number
}

export function getConfiguration(): Configuration {
	return {
		host: String(process.env.HOSTNAME),
		port: Number(process.env.PORT)
	};
}