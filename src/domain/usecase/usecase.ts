export interface Usecase {
	execute(...args: unknown[]): Promise<unknown>
}