import { User } from "../entity/user";
import { Usecase } from "./usecase";

export class SayHelloUsecase implements Usecase {
	constructor(private readonly userRepository: User.Repository) {
	}

	async execute(): Promise<string> {
		const user = (await this.userRepository.getOne(`1`))[0];
		return `Hello ${user.getFullName()}`;
	}
}