import { UserSql } from '../../infrastructure/model/user.model';

export class User {
	constructor(
		private readonly id: string,
		private readonly firstname: string,
		private readonly lastname: string
	) {
	}

	public getFullName(): string {
		return `${this.firstname} ${this.lastname}`;
	}
}

export namespace User {
	export interface Repository {
		getOne: (id: string) => Promise<User[]>
	}

	export function toUser(userSql: UserSql): User {
		return new User(String(userSql.id), userSql.firstName, userSql.lastName);
	}
}
