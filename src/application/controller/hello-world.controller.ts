import { Request, ResponseObject, ResponseToolkit } from "@hapi/hapi";

import { SayHelloUsecase } from "../../domain/usecase/say-hello.usecase";

export class HelloWorldController {
	constructor(private readonly sayHelloUsecase: SayHelloUsecase) {
	}

	public async sayHello(request: Request, toolkit: ResponseToolkit): Promise<ResponseObject> {
		const helloAndName = await this.sayHelloUsecase.execute();
		return toolkit.response(helloAndName).code(200);
	}
}