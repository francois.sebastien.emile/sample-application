import { ServerRoute } from "@hapi/hapi";
import { HelloWorldController } from "../controller/hello-world.controller";

export class HelloWorldEndpoint {
	private routes: ServerRoute[]

	constructor(private readonly helloWorldController: HelloWorldController) {
		this.initRoutes();
	}

	private initRoutes(): void {
		this.routes = [{
			path: `/hello-world`,
			method: `GET`,
			handler: this.helloWorldController.sayHello.bind(this.helloWorldController)
		}];
	}

	public getRoutes(): ServerRoute[] {
		return this.routes;
	}
}
