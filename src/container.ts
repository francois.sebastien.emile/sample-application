import { getCustomRepository } from 'typeorm';
import { HelloWorldController } from "./application/controller/hello-world.controller";
import { HelloWorldEndpoint } from './application/endpoint/hello-world.endpoint';
import { User } from './domain/entity/user';
import { SayHelloUsecase } from "./domain/usecase/say-hello.usecase";
import { UserSqlRepository } from './infrastructure/repository/user-sql.repository';

export interface Dependencies {
	HelloWorldEndpoint: HelloWorldEndpoint
}

export class Container {
	constructor(private readonly dependencies: Dependencies) {
	}

	public getDependency<InstanceType extends Dependencies[keyof Dependencies]>(
		dependencyName: keyof Dependencies
	): InstanceType {
		return this.dependencies[dependencyName] as InstanceType;
	}

	public static create(): Container {
		const userRepository: User.Repository = getCustomRepository(UserSqlRepository);
		const sayHelloUsecase: SayHelloUsecase = new SayHelloUsecase(userRepository);
		const helloWorldController: HelloWorldController = new HelloWorldController(sayHelloUsecase);
		const helloWorldEndpoint: HelloWorldEndpoint = new HelloWorldEndpoint((helloWorldController));

		const dependencies: Dependencies = {
			HelloWorldEndpoint: helloWorldEndpoint
		};
		return new Container(dependencies);
	}
}
