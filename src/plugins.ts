import Inert from "@hapi/inert";
import Vision from "@hapi/vision";
import HapiSwagger from "hapi-swagger";

function getSwaggerOptions() {
	return {
		info: {
			title: `API Name`,
		}
	};
}

export function getPlugins(): any {
	return [
		Inert,
		Vision,
		{
			plugin: HapiSwagger,
			options: getSwaggerOptions()
		}];
}
