import { Server, ServerRegisterPluginObject, ServerRoute } from "@hapi/hapi";
import { Connection, createConnection } from 'typeorm';
import { HelloWorldEndpoint } from './application/endpoint/hello-world.endpoint';

import { Configuration, getConfiguration } from "./config";
import { Container } from './container';
import { getPlugins } from "./plugins";

class Application {
	private readonly container: Container
	private readonly plugins?: ServerRegisterPluginObject<unknown>[]
	private routes: ServerRoute[]
	private readonly server: Server

	private constructor(
		configuration: Configuration,
		plugins?: ServerRegisterPluginObject<unknown>[]
	) {
		this.container = Container.create();
		this.server = new Server({
			host: configuration.host,
			port: configuration.port,
		});
		if (plugins) {
			this.plugins = plugins;
		}
	}

	public static async run(
		configuration: Configuration,
		routes: ServerRoute[],
		plugins?: ServerRegisterPluginObject<unknown>[]
	): Promise<Application> {
		const application = new Application(configuration, plugins);
		await application.start();
		return application;
	}

	private async start(): Promise<void> {
		this.server.validator(require(`@hapi/joi`));
		this.registerRoutes();
		if (this.plugins) {
			await this.server.register(this.plugins);
		}
		await this.server.start();
	}

	private registerRoutes(): void {
		this.routes = [
			...this.container.getDependency<HelloWorldEndpoint>(`HelloWorldEndpoint`).getRoutes()
		];
		this.server.route(this.routes);
	}
}

createConnection()
	.then((_connection: Connection) => {
		Application
			.run(getConfiguration(), getPlugins())
			.then((_application: Application) => {
				console.log(`Application has started`);
			})
			.catch((error) => console.log(error));
	})
	.catch((error) => console.log(error));
