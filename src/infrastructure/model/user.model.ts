import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: `users` })
export class UserSql {

	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	firstName: string;

	@Column()
	lastName: string;

	@Column()
	isActive: boolean;

}