import { EntityRepository, Repository } from 'typeorm';
import { User } from '../../domain/entity/user';
import { UserSql } from '../model/user.model';

@EntityRepository(UserSql)
export class UserSqlRepository extends Repository<UserSql> implements User.Repository {
	async getOne(id: string): Promise<User[]> {
		return (await this.findByIds([id])).map(User.toUser);
	}
}