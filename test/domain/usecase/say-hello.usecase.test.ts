import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import { expect } from "chai";
import * as sinon from 'sinon';

import { User } from '../../../src/domain/entity/user';
import { SayHelloUsecase } from '../../../src/domain/usecase/say-hello.usecase';

describe(`SayHelloUsecase`, () => {
	
	let sandbox: sinon.SinonSandbox;

	describe(`.execute()`, function () {

		beforeEach(() => {
			sandbox = sinon.createSandbox();
		});

		afterEach(() => {
			sandbox.restore();
		});

		it(`returns toto`, async () => {
			// Given
			const user = new User( `1`, `Kakashi`, `Hatake` );
			const userRepository: StubbedType<User.Repository> = stubInterface<User.Repository>(sandbox);
			userRepository.getOne.resolves([user]);
			const sayHelloUsecase: SayHelloUsecase = new SayHelloUsecase(userRepository);

			// When
			const result = await sayHelloUsecase.execute();

			// Then
			expect(result).to.deep.equal(`Hello Kakashi Hatake`);
		});
	});

});
